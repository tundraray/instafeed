const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');

const VkBot = require('./bot');
const vkapi = require('./libs/vk-wrapper');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());


// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);



// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({
  logger
}));

const bot = VkBot({
  token: app.get('token'),
  group_id: app.get('group_id')
});
const api = new vkapi({
  token: app.get('token'),
  group_id: app.get('group_id')
});

/*
const defaultError = (ctx) => ctx.reply('Упс! Что-то пошло не так. Может стоит поискать котиков?', null, Markup
  .keyboard([
    'Котики',
    'Корги',
    'Енотики',
  ]).oneTime());
const service = require('./services/groupfeed/groupfeed.class');
bot.on((ctx) => {
  const {
    text
  } = ctx.message;
  //console.log(ctx.message.attachments[0].photo.sizes)
  if (!text) {
    defaultError(ctx);
    return;
  }
  new service().find({
    query: {
      find: text
    }
  }).then(item => {
    ctx.reply('Шур-шур-шур! Начинаю поиск. Ж-Ж-Ж!');
    if(!item.images) {
      ctx.reply('Черт! Ничего не нашел :(');
    }

    api.uploadPhotosByUrls(ctx.message.from_id, item.images).then(response => {
      ctx.reply('Ура! Я закончил! Смотри, что я нашел!', response.join(','));
    }).catch(err => {
      console.log(err);
      return defaultError(ctx);
    });
  }).catch(err => {
    console.log(err);
    return defaultError(ctx);
  });

});

*/

app.hooks(appHooks);
bot.startPolling();

module.exports = app;
