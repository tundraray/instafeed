const VkBot = require('node-vk-bot-api');
const Session = require('node-vk-bot-api/lib/session');
const Markup = require('node-vk-bot-api/lib/markup');
const scene = require('../libs/bot/scene');
const Stage = require('node-vk-bot-api/lib/stage');
const vkapi = require('../libs/vk-wrapper');

const vinci = new(require('../libs/vinci'))();
const _ = require('lodash');

module.exports = (options) => {
  const bot = new VkBot(options);
  const api = new vkapi(options);

  const session = new Session();
  const stage = new Stage(scene(api));

  bot.use(session.middleware());
  bot.use(stage.middleware());

  //bot.command('Начать', (ctx) => {
  //  ctx.scene.enter('meet');
  //});

  bot.on((ctx) => {
    //console.log(ctx)
    const {
      text
    } = ctx.message;
    const photos = ctx.message.attachments.filter(f => f.type === 'photo');

    if (text === 'Начать') {
      ctx.reply('Привет, я обновил себе прошивку! Загрузи мне картинку, и я добавлю щепотку магии.');
      return;
    }    

    if (photos && photos.length > 0) {

      const prom = [];
      ctx.message.attachments.forEach(element => {
        if (element.type === 'photo') {
          const photo = _.orderBy(element.photo.sizes, ['type'], ['desc'])[0];
          if (photo) {
            prom.push(vinci.uploadPhotoByUrl(photo.url)
              .then(res => vinci.getById(res.preload))
              .then(buffer => api.uploadPhotoByFile(ctx.message.from_id, buffer)).catch(console.error));
          }
        }
      });
      Promise.all(prom).then(files => {
        const attachments = files.map(res => `photo${res[0].owner_id}_${res[0].id}`).join(',');
        ctx.reply('Вупс-картинка-бумс! Получилось!', attachments);
      });
      ctx.reply('Одну минуту: читаю магическую книгу!');
    } else {
      ctx.reply('Я могу работать только с картинками. Так что скорее загружай, и я добавлю щепотку магии.');
    }

    
  });

  return bot;
};
