const vk = require('vk-call').VK;
const fetch = require('node-fetch');
const FormData = require('form-data');

class ApiWrapper {
  constructor(options) {
    this.options = options || {};
    this.api = new vk({
      version: '5.80',
      timeout: 10000,
      ...this.options,
    });
  }


  async uploadPhotoByUrl(user_id, url) {
    try {
      var downloader = await fetch(url);

      const file = await downloader.buffer();
      return await this.uploadPhotoByFile(user_id, file);
    } catch (error) {
      throw error;
    }
  }
  async uploadPhotosByUrls(user_id, urls) {
    try {
      const result = await  Promise.all(urls.map(url => this.uploadPhotoByUrl(user_id,url)));
      return result.map(res=> `photo${res[0].owner_id}_${res[0].id}`);
    } catch (error) {
      //console.error(error);
      throw error;
    }

  }

  async uploadPhotoByFile(user_id, file) {
    try {
      const uploadConfiguration = await this.api.call('photos.getMessagesUploadServer', {
        peer_id: user_id
      });
      console.log(file.length);
      const formData = FormData();
      formData.append('photo', file,'photo.jpg');
      var uploader = await fetch(uploadConfiguration.upload_url, { // Your POST endpoint
        method: 'POST',
        headers: formData.getHeaders(),
        body: formData // This is your file object
      });
      const fileResponse = await uploader.json();
      console.log(fileResponse)
      return await this.api.call('photos.saveMessagesPhoto', fileResponse);
    } catch (error) {
      //console.error(error);
      throw error;
    }
  }
}

module.exports = ApiWrapper;
