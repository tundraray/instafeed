const RSS = require('rss-generator');

const feeds = {};
module.exports = (tag, categories) => {
  if (!feeds[tag]) {
    feeds[tag] = new RSS({
      title: 'Instafeed',
      language: 'en',
      ttl: '60',
      categories: categories,
      custom_namespaces: {
        'media': 'http://search.yahoo.com/mrss/'
      },
    });
  }
  feeds[tag].items = [];
  return feeds[tag];
};
