const Scene = require('node-vk-bot-api/lib/scene');
const Markup = require('node-vk-bot-api/lib/markup');
const vinci = new(require('../../vinci'))();
const _ = require('lodash');
const scene = (vkapi) => new Scene('meet',
  (ctx) => {
    if (ctx.message.text === 'Я передумал') {
      ctx.scene.leave();

      ctx.reply('Жаль, ну ты пиши, если что. Я пока поковыряю прошивку.', null, Markup
        .keyboard([
          Markup.button('Приветствовать бота снова', 'positive', {
            button: 'Начать'
          }),
        ]));
      return;
    }
    ctx.scene.next();
    ctx.reply('И так, я учусь обрабатывать фотографии. Давай попробуем, загрузи фотографию, а добавлю магию!');
  },
  (ctx) => {
    const photos = ctx.message.attachments.filter(f => f.type === 'photo');

    if (photos && photos.length > 0) {

      const prom = [];
      ctx.message.attachments.forEach(element => {
        if (element.type === 'photo') {
          const photo = _.orderBy(element.photo.sizes, ['type'], ['desc'])[0];
          if (photo) {
            prom.push(vinci.uploadPhotoByUrl(photo.url)
              .then(res => vinci.getById(res.preload))
              .then(buffer => vkapi.uploadPhotoByFile(ctx.message.from_id, buffer)).catch(console.error));
          }
        }
      });
      Promise.all(prom).then(files => {
        const attachments = files.map(res => `photo${res[0].owner_id}_${res[0].id}`).join(',');
        ctx.reply('Ура! Я закончил! Я молодец?', attachments);
      });
      ctx.reply('Одну минуту!');
      ctx.scene.leave();
    } else {
      ctx.reply('Я не нашел фотографии, если ты не передумал, то скорее загружай! Или жмакни кнопку.', null, Markup
        .keyboard([
          Markup.button('Я передумал', 'negative'),
        ]).oneTime());
      ctx.scene.selectStep(0);
    }

  });

module.exports = scene;
