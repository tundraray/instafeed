const fetch = require('node-fetch');
const FormData = require('form-data');
const _ = require('lodash');

const filters = [{
  'id': '2'
},
{
  'id': '56'
}, {
  'id': '4'
}, {
  'id': '52'
}, {
  'id': '3'
}, {
  'id': '34'
}, {
  'id': '7'
}, {
  'id': '26'
}, {
  'id': '10'
}, {
  'id': '21'
}, {
  'id': '1'
}, {
  'id': '27'
}, {
  'id': '64'
}, {
  'id': '36'
}, {
  'id': '51'
}, {
  'id': '29'
}, {
  'id': '44'
}, {
  'id': '37'
}, {
  'id': '45'
}, {
  'id': '59'
}, {
  'id': '60'
}, {
  'id': '57'
}, {
  'id': '58'
}, {
  'id': '38'
}, {
  'id': '46'
}, {
  'id': '48'
}, {
  'id': '4201'
}, {
  'id': '39'
}, {
  'id': '12'
}, {
  'id': '31'
}, {
  'id': '28'
}, {
  'id': '5'
}, {
  'id': '24'
}, {
  'id': '15'
}, {
  'id': '9'
}
];


class Vinci {
  constructor(options) {
    this.options = options;
  }

  
  async getById(id, filter) {
    if(!filter) {
      filter = filters[_.random(filters.length - 1)].id;
    }
    try {
      var downloader = await fetch(`http://vinci.camera/process/${id}/${filter}?face=0&lang=ru`, {
        method: 'GET',
        headers: {
          'User-Agent': 'vinci_android 2.0.5 f8b53e65971af0c2 6033e4b30fa0f50830c6fba46c430ef95B806364'
        },
        mode: 'cors',
        cache: 'default'
      });
      return await downloader.buffer();
    } catch (error) {
      throw error;
    }
  }
  async uploadPhotoByUrl(url) {
    try {
      var downloader = await fetch(url);

      const file = await downloader.buffer();
      return await this.uploadPhotoByFile(file);
    } catch (error) {
      throw error;
    }
  }
  async uploadPhotoByFile(file) {
    try {

      const formData = FormData();
      formData.append('photo', file, 'photo.jpg');
      var uploader = await fetch('http://vinci.camera/preload', { // Your POST endpoint
        method: 'POST',
        headers: formData.getHeaders(),
        body: formData // This is your file object
      });
      return await uploader.json();
    } catch (error) {
      //console.error(error);
      throw error;
    }
  }

}

module.exports = Vinci;
