/* eslint-disable no-unused-vars */
const Instagram = require('../../libs/instagram');
const _ = require('lodash');

class Service {
  constructor(options) {
    this.options = options || {};
    this.instagram = new Instagram();
  }

  find(params) {
    const {
      query
    } = params;
    return new Promise((resolve, reject) => {
      this.instagram.searchBy('hashtag', encodeURI(query.find)).then(result => {
        var date = new Date();
        date.setMinutes(0,0,0);
   
        if(!result) {
          resolve({
            date: parseInt(date.getTime() / 1000),
            pubdate: parseInt(date.getTime() / 1000) + (60*60*3),
            images: []
          });
          return;
        }
        
        const {
          entry_data: {
            TagPage
          }
        } = result;

        let items = _.get(TagPage, ['0', 'graphql', 'hashtag', 'edge_hashtag_to_top_posts', 'edges'],[]).map(item => {
          const {
            node
          } = item;
          return node;
        });

        items = _.filter(items, (o) => {
          return o.__typename != 'GraphVideo';
        });

        resolve({
          date: parseInt(date.getTime() / 1000),
          pubdate: parseInt(date.getTime() / 1000) + (60*60*3),
          images: items.map(item => item.display_url)
        });
      }).catch(reject);
    });

  }


  getAutorName(id) {
    return new Promise((resolve, reject) => {
      new Instagram().getMediaInfoByUrl(`https://www.instagram.com/p/${id}`).then(result => {
        this.get(result.author_name).then(resolve).catch(reject);
      }).catch(reject);
    });
  }

  get(id, params) {

    return new Promise((resolve, reject) => {
      new Instagram().getUserDataByUsername(id).then(result => {
        const {
          graphql: {
            user
          }
        } = result;
        const items = user.edge_owner_to_timeline_media.edges.map(item => {
          const {
            node
          } = item;
          return node;
        });
        resolve(items);
      }).catch(reject);
    });
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
