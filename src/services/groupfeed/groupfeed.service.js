// Initializes the `groupfeed` service on path `/groupfeed`

const createService = require('./groupfeed.class.js');
const hooks = require('./groupfeed.hooks');
const feed = require('../../libs/feeds');



module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/groupfeed', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('groupfeed');

  app.use('/groupfeed.rss', service, function (req, res) {
    res.format({
      'text/xml': function () {
        const {
          category,
          find,
          tag,
          community
        } = req.query;
        let _feed = feed(find, category ? [category] : []);
        const item = res.data;
        const custom_elements = item.images.map(item => `<img src="${item}">`);


        _feed.item({
          title: `${item.date}`,
          description: `#${tag || find}@${community || 'myinteriormag'} \n\n ${custom_elements.join('')}`,
          date: new Date(item.date * 1000),
          guid: `${item.date}`,
          link: `${item.date}`,
        });

        res.end(_feed.xml());
      }
    });
  });

  service.hooks(hooks);
};
