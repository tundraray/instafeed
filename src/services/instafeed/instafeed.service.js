// Initializes the `instafeed` service on path `/instafeed`
// Initializes the `deepfeed` service on path `/deepfeed`
const _ = require('lodash');
const createService = require('./instafeed.class.js');
const hooks = require('./instafeed.hooks');

const feed = require('../../libs/feeds');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/instafeed', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('instafeed');

  app.use('/instafeed.rss', service, function (req, res) {
    res.format({
      'text/xml': function () {
        const { category, find, tag, community } = req.query;
        let _feed = feed(find, category ? [category] : []);
        res.data.forEach(item => {
          let text = _.get(item, ['edge_media_to_caption','edges','0','node','text'],'') ;
          if(find) {
            text = `#${tag || find}@${community || 'myinteriormag'}`;
          }
          let content = [];
          if(item.images) {
            content = item.images.map(item => `<img src="${item}">`);
          } else {
            content.push(`<img src="${item.display_url}">`);
          }
          _feed.item({
            title: `${item.id}`,
            description: text + content.join(''),
            date: new Date(item.taken_at_timestamp*1000),
            guid: `https://www.instagram.com/p/${item.shortcode}`,
            link: `https://www.instagram.com/p/${item.shortcode}`,
          });
        });
        res.end(_feed.xml());
      }
    });
  });

  service.hooks(hooks);
};
