/* eslint-disable no-unused-vars */
const Instagram = require('../../libs/instagram');
const _ = require('lodash');

class Service {
  constructor(options) {
    this.options = options || {};
    this.instagram = new Instagram();
  }

  find(params) {
    const {
      query
    } = params;
    return new Promise((resolve, reject) => {
      this.instagram.searchBy('hashtag', query.find).then(result => {
        const {
          entry_data: {
            TagPage
          }
        } = result;

        let items = _.get(TagPage, ['0', 'graphql', 'hashtag', 'edge_hashtag_to_top_posts', 'edges'],[]).map(item => {
          const {
            node
          } = item;
          return node;
        });

        items = _.filter(items, (o) => {
          return o.__typename != 'GraphVideo';
        }).map(item => this.instagram.getMediaById(item.shortcode));

        Promise.all(items).then(authorPosts => {
          const posts = authorPosts.map(post => {
            let result = _.get(post,['entry_data','PostPage','0','graphql','shortcode_media'],{});
            if(result.edge_sidecar_to_children) {
              result.images = result.edge_sidecar_to_children.edges.map(egde => egde.node.display_url);
            }
            return result;
          });
          resolve(_.take(_.orderBy(posts, ['taken_at_timestamp','edge_liked_by.count'], ['desc','desc']),20));
        });
      }).catch(reject);
    });

  }


  getAutorName(id) {
    return new Promise((resolve, reject) => {
      new Instagram().getMediaInfoByUrl(`https://www.instagram.com/p/${id}`).then(result => {
        this.get(result.author_name).then(resolve).catch(reject);
      }).catch(reject);
    });
  }

  get(id, params) {

    return new Promise((resolve, reject) => {
      new Instagram().getUserDataByUsername(id).then(result => {
        const {
          graphql: {
            user
          }
        } = result;
        const items = user.edge_owner_to_timeline_media.edges.map(item => {
          const {
            node
          } = item;
          return node;
        });
        resolve(items);
      }).catch(reject);
    });
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
