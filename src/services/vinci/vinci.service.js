// Initializes the `vinci` service on path `/filter/vinci`
const createService = require('./vinci.class.js');
const hooks = require('./vinci.hooks');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    type: 'jpg',
    cache: false,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/filter/vinci', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('filter/vinci');
 

  service.hooks(hooks);
};
