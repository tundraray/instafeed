const instafeed = require('./instafeed/instafeed.service.js');
const deepfeed = require('./deepfeed/deepfeed.service.js');
const groupfeed = require('./groupfeed/groupfeed.service.js');
const vinci = require('./vinci/vinci.service.js');
const imagesVinci = require('./images-vinci/images-vinci.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(instafeed);
  app.configure(deepfeed);
  app.configure(groupfeed);
  app.configure(vinci);
  app.configure(imagesVinci);
};
