// Initializes the `images-vinci` service on path `/images/vinci`
const createService = require('./images-vinci.class.js');
const hooks = require('./images-vinci.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/images/vinci', createService(options), function (req, res) {
   
    res.format({
      'image/jpeg': function () {
        res.contentType('image/jpeg');
        res.set({ 'content-type': 'image/jpeg' });
        res.end(res.data, 'binary');
      }
    });
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('images/vinci');

  service.hooks(hooks);
};
