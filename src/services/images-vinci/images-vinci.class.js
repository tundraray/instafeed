/* eslint-disable no-unused-vars */
const fetch = require('node-fetch');
class Service {
  constructor (options) {
    this.options = options || {};
  }


  get(id, params) {
    const {
      query: { filter = 1 },
    } = params;

    return new Promise((resolve, reject) => {
      fetch(`http://vinci.camera/process/${id}/${filter}?face=0&lang=ru`, {
        method: 'GET',
        headers: {
          'User-Agent': 'vinci_android 2.0.5 f8b53e65971af0c2 6033e4b30fa0f50830c6fba46c430ef95B806364'
        },
        mode: 'cors',
        cache: 'default'
      }).then(res => res.buffer())
        .then(resolve)
        .catch(e => {
          reject(e);
        });
    });
  }

  create(data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return Promise.resolve(data);
  }

}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
