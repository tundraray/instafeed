// Initializes the `deepfeed` service on path `/deepfeed`
const _ = require('lodash');
const createService = require('./deepfeed.class.js');
const hooks = require('./deepfeed.hooks');

const feed = require('../../libs/feeds');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/deepfeed', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('deepfeed');

  app.use('/deepfeed.rss', service, function (req, res) {
    res.format({
      'text/xml': function () {
        const { category, find, tag, community } = req.query;
        let _feed = feed(category ? [category] : []);
        res.data.forEach(item => {
          let text = _.get(item, ['edge_media_to_caption','edges','0','node','text'],'') ;
          if(find) {
            text = `#${tag || find}@${community || 'myinteriormag'}`;
          }
          _feed.item({
            title: `${item.id}`,
            description: text,
            date: new Date(item.taken_at_timestamp*1000),
            guid: `https://www.instagram.com/p/${item.shortcode}`,
            link: `https://www.instagram.com/p/${item.shortcode}`,
            custom_elements: [{
              'media:content': {
                _attr: {
                  url: item.display_url
                }
              }
            },

            ]
          });
        });
        res.end(_feed.xml());
      }
    });
  });

  service.hooks(hooks);
};
