/* eslint-disable no-unused-vars */
const Instagram = require('instagram-nodejs-without-api');
const _ = require('lodash');

class Service {
  constructor(options) {
    this.options = options || {};
    this.instagram = new Instagram();
  }

  find(params) {
    const {
      query
    } = params;
    return new Promise((resolve, reject) => {
      this.instagram.searchBy('hashtag', query.find).then(result => {
        const { entry_data: { TagPage } } = result;
        const items = _.get(TagPage, ['0', 'graphql', 'hashtag', 'edge_hashtag_to_top_posts', 'edges'],[]).map(item => {
          const { node } = item;
          return this.getAutorName(node.shortcode);
        });      
        Promise.all(items).then(authorPosts => {
          let final = [];
          authorPosts.forEach(posts => {
            final = [...final,..._.filter(posts, (o) => { 
              return o.__typename != 'GraphVideo' && o.edge_liked_by.count > 900 && o.edge_media_to_comment.count > 20; 
            })];
          });
          
          resolve(_.take(_.orderBy(final, ['taken_at_timestamp','edge_liked_by.count'], ['desc','desc']),20));
        }).catch(reject); 
        //resolve(items);
      }).catch(reject);
    });

  }
  

  getAutorName(id) {
    return new Promise((resolve, reject) => {
      new Instagram().getMediaInfoByUrl(`https://www.instagram.com/p/${id}`).then(result => {  
        this.get(result.author_name).then(resolve).catch(reject);     
      }).catch(reject);
    });
  }

  get(id, params) {

    return new Promise((resolve, reject) => {
      new Instagram().getUserDataByUsername(id).then(result => {
        const { graphql: { user } } = result;
        const items = user.edge_owner_to_timeline_media.edges.map(item => {
          const { node } = item;
          return node;
        });       
        resolve(items);
      }).catch(reject);
    });
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
