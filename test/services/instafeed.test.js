const assert = require('assert');
const app = require('../../src/app');

describe('\'instafeed\' service', () => {
  it('registered the service', () => {
    const service = app.service('instafeed');

    assert.ok(service, 'Registered the service');
  });
});
