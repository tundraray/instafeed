const assert = require('assert');
const app = require('../../src/app');

describe('\'images-vinci\' service', () => {
  it('registered the service', () => {
    const service = app.service('images/vinci');

    assert.ok(service, 'Registered the service');
  });
});
