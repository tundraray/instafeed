const assert = require('assert');
const app = require('../../src/app');

describe('\'vinci\' service', () => {
  it('registered the service', () => {
    const service = app.service('filter/vinci');

    assert.ok(service, 'Registered the service');
  });
});
