const assert = require('assert');
const app = require('../../src/app');

describe('\'vinci-image\' service', () => {
  it('registered the service', () => {
    const service = app.service('image/vinci');

    assert.ok(service, 'Registered the service');
  });
});
