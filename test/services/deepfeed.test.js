const assert = require('assert');
const app = require('../../src/app');

describe('\'deepfeed\' service', () => {
  it('registered the service', () => {
    const service = app.service('deepfeed');

    assert.ok(service, 'Registered the service');
  });
});
