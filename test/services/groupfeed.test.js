const assert = require('assert');
const app = require('../../src/app');

describe('\'groupfeed\' service', () => {
  it('registered the service', () => {
    const service = app.service('groupfeed');

    assert.ok(service, 'Registered the service');
  });
});
