FROM node:alpine


ENV site /build
ADD ./ $site
WORKDIR $site

RUN cd /build && npm install && \
    npm cache clean --force

EXPOSE 3030

ENTRYPOINT ["npm", "start"]